# Proj0-Hello
-------------

Author: Bethany Van Meter
Contact: bvanmet2@uoregon.edu
  BitBucket: bethanyvanmeter
Description: Software runs a python script that prints "Hello world"

## Instructions:
---------------

 Use the command ``make run`` to execute. 
